#!/usr/bin/env python3

import sys
import argparse
import re
import serial

class HwId:
    short_name = "HW ID"
    long_name = "Hardware ID"
    short_in_opt = "i"
    long_in_opt = "hwid"
    out_opt = "I"
    
    @staticmethod
    def encode(hardware_id):
        return hardware_id.decode('ascii').upper()

    @staticmethod
    def decode(hardware_id):
        if re.search("^\d{2}[0-9A-Fa-f]{8}$", hardware_id) == None:
            sys.exit("Error: " + hardware_id
                     + " is not a valid hardware ID")
        return hardware_id
        
class RcoCoding:
    short_name = "RCO"
    long_name = "RCO"
    short_in_opt = "r"
    long_in_opt = "rco"
    out_opt = "R"
    
    @staticmethod
    def encode(hardware_id):
        return "%04d%05d" % (int(hardware_id[2:6], 16),
                             int(hardware_id[6:10], 16))

    @staticmethod
    def decode(rco):
        if re.search("^\d{9}$", rco) == None:
            sys.exit("Error: " + rco + " is not a valid RCO ID")
        if args.prefix == None:
            print("Warning: Assuming HW ID prefix 01")
            prefix = '01'
        else:
            prefix = args.prefix
        return "%s%04X%04X" % (prefix, int(rco[0:4]), int(rco[4:9]))
        
class BewCoding:
    short_name = "BEW"
    long_name = "Bewator (Siemens)"
    short_in_opt = "b"
    long_in_opt = "bew"
    out_opt = "B"
    
    @staticmethod
    def encode(hardware_id):
        return "%03d%010d" % (int(hardware_id[0:2], 16),
                              int(hardware_id[2:], 16))

    @staticmethod
    def decode(bew):
        if len(bew) != 13:
            bew = '001' + bew
        if re.search("^\d{13}$", bew) == None:
            sys.exit("Error: " + bew + " is not a valid Bewator ID")
        return "%02X%08X" % (int(bew[0:3]), int(bew[3:]))

class SolCoding:
    short_name = "SOL"
    long_name = "Solid"
    short_in_opt = "s"
    long_in_opt = "sol"
    out_opt = "S"
    
    @staticmethod
    def encode(hardware_id):
        binRevBytes = ''
        for i in range(2, len(hardware_id), 2):
            binRevBytes += bin(int(hardware_id[i:i+2], 16))[2:].zfill(8)[::-1]
        return "%010d" % int(binRevBytes, 2)

    @staticmethod
    def decode(sol):
        if re.search("^\d{10}$", sol) == None:
            sys.exit("Error: " + sol + " is not a valid Solid ID")
        hex_in = "%08X" % int(sol)
        binRevBytes = ''
        for i in range(0, len(hex_in), 2):
            binRevBytes += bin(int(hex_in[i:i+2], 16))[2:].zfill(8)[::-1]
        print("Warning: Assuming HW ID prefix 01")
        return "01%08X" % int(binRevBytes, 2)
        
class ACoding:
    short_name = "A"
    long_name = "Aptus"
    short_in_opt = "a"
    long_in_opt = "a"
    out_opt = "A"
    
    @staticmethod
    def encode(hardware_id):
        # FIXME: confirm how to really do this:
        # 4453488506 is for example changed to 453488506
        # Should we always just skip the first digit? Is it always
        # limited to 9 digits? Should it be zero-padded?
        return str(int(hardware_id, 16))[-9:].zfill(9)

    @staticmethod
    def decode(a):
        if re.search("^\d{9}$", a) == None:
            sys.exit("Error: " + a + " is not a valid A ID")
        print("Warning: Assuming HW ID prefix 4 (dec)")
        return "%010X" % int('4' + a)

def print_formatted_id(hw_id):
    for c in classes:
        if eval('args.' + c.out_opt) or n_out_formats == 0:
            if n_out_formats != 1 or not args.minimal:
                print(c.short_name + ": ", end="")
            print(c.encode(hw_id))

parser = argparse.ArgumentParser(description='''Convert between
                                 different proximity card ID
                                 representations. Supported formats are
                                 (except the raw hardware ID) RCO, BEW,
                                 SOL and A.
                                 
                                 Either an input value or a file has to
                                 be provided.''')
classes = [HwId, RcoCoding, BewCoding, SolCoding, ACoding]
in_group = parser.add_mutually_exclusive_group(required=True)
in_group.add_argument('-d', '--device')
out_group = parser.add_argument_group("Output formats")
for c in classes:
    in_group.add_argument('-' + c.short_in_opt, '--' + c.long_in_opt,
                          help=c.long_name, nargs='?', const='')
    out_group.add_argument('-' + c.out_opt, action='store_true',
                           help=c.short_name)
parser.add_argument('--minimal', action='store_true')
parser.add_argument('--prefix')
parser.add_argument('-f', '--file', nargs='?',
                    type=argparse.FileType('r'), const=sys.stdin,
                    help='Default: stdin')

args = parser.parse_args()
n_out_formats = 0
for c in classes:
    if eval('args.' + c.out_opt):
        n_out_formats += 1

if args.device != None:
    ser = serial.Serial(args.device)
    string = b""
    first_entry = True;
    while True:
        try:
            b = ser.read()
        except:
            ser.close()
            sys.exit()
        if b == b'\x02': # STX
            string = b""
            b = ser.read()
            length = int.from_bytes(b, "big")
            if b >= b'0':
                string += b
                # Reader type: Promag PCR300/330
                continue
            # Reader type: Gwiot 7941E
            ser.read() # Card type
            card_id = ser.read(length-5)
            ser.read() # Checksum
            ser.read() # End
            if not first_entry and n_out_formats != 1:
                print('')
            print_formatted_id(card_id.hex().encode('ascii'))
            first_entry = False;
        elif b == b'\x1b': # Escape
            string = b""
        elif b == b'\r' or b == b'\n':
            if string != b"":
                if not first_entry and n_out_formats != 1:
                    print('')
                print_formatted_id(string)
            string = b""
            first_entry = False;
        else:
            string += b
else:
    for c in classes:
        id_in = eval('args.' + c.long_in_opt)
        if id_in == '': # Found an input option without value, read from file
            if (args.file == None):
                sys.exit("Need one input value or file to read from")
            first_entry = True;
            while True:
                try:
                    line = args.file.readline()
                except KeyboardInterrupt:
                    sys.exit()
                if not line:
                    sys.exit()
                hw_id = c.decode(line.strip())
                print_formatted_id(hw_id)
             #   if not first_entry and n_out_formats != 1:
                print('')
                first_entry = False;
        elif id_in != None:
            hw_id = c.decode(id_in)
            print_formatted_id(hw_id)
            sys.exit()
