# Prox key utility
`prox.py` is a tool to read proximity keys (RFID) and convert between different formats.

## Usage
```
usage: prox.py [-h] [-d DEVICE] [-i [HWID]] [-I] [-r [RCO]] [-R] [-b [BEW]] [-B]
               [-s [SOL]] [-S] [-a [A]] [-A] [--minimal] [--prefix PREFIX]
               [-f [FILE]]

Convert between different proximity card ID representations. Supported formats
are (except the raw hardware ID) RCO, BEW, SOL and A.

Either an input value or a file has to be provided.

optional arguments:
  -h, --help            show this help message and exit
  -d DEVICE, --device DEVICE
  -i [HWID], --hwid [HWID]
                        Hardware ID
  -r [RCO], --rco [RCO]
                        RCO
  -b [BEW], --bew [BEW]
                        Bewator (Siemens)
  -s [SOL], --sol [SOL]
                        Solid
  -a [A], --a [A]       Aptus
  --minimal
  --prefix PREFIX
  -f [FILE], --file [FILE]
                        Default: stdin

Output formats:
  -I                    HW ID
  -R                    RCO
  -B                    BEW
  -S                    SOL
  -A                    A
```

Example: reading from a connected prox key reader:
```
./prox.py -d /dev/ttyUSB0
```

## Requirements
* Python 3
* pyserial
* A supported reader (if you want to read from physical keys). Supported readers are
    - Promag PCR300/330
    - Gwiot 7941E (via UART)

## License
This tool is licensed under the MIT License (see LICENSE).
